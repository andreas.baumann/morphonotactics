library(mgcv)
library(lmtest)


# data from Baumann & Matzinger (2021, Language Sciences)
df = read.delim("/cloud/project/morphonotactics/data/diachronic_data.csv")

# normalize to unit interval
normalize = function(x, na.rm = TRUE) {
  return((x-min(x)) /(max(x)-min(x)))
}

syn = normalize(df$syntheticity)
pt_div = normalize(df$diversity1)
pt_num = normalize(df$diversity0)

# granger tests: syn vs. pt diversity
grangertest(pt_div ~ syn, order = 1) # pt predicted by syntheticity
grangertest(syn ~ pt_div, order = 1) # syntheticity predicted by pt diversity


# visualization of trajectories
dev.off()
par(mfrow = c(1,2))

plot(syn, type = "l", col = "orange", lwd = 2, xlab = "time (semicentury)", ylab = "syntheticity / phonotactic div", bty = "n")
lines(pt_div, type = "l", col = "darkmagenta", lwd = 2)

# syn vs. pt inventory size
grangertest(pt_num ~ syn, order = 1) # pt predicted by syntheticity
grangertest(syn ~ pt_num, order = 1) # syntheticity predicted by pt inventory size

plot(syn, type = "l", col = "orange", lwd = 2, xlab = "time (semicentury)", ylab = "syntheticity / phonotactic num", bty = "n")
lines(pt_num, type = "l", col = "darkmagenta", lwd = 2)

# syn vs. pt diversity (model based)
par(mfrow = c(1,1))

time = 1:length(syn)

# granger test model based
new_time = seq(1, 17, 1)
syn_mdl = predict(gam(syn ~ s(time, k = 10)), newdata = data.frame(time = new_time))
pt_div_mdl = predict(gam(pt_div ~ s(time, k = 10)), newdata = data.frame(time = new_time))

grangertest(pt_div_mdl ~ syn_mdl, order = 1) # pt predicted by syntheticity
grangertest(syn_mdl ~ pt_div_mdl, order = 1) # syntheticity predicted by pt

# correlation
cor.test(syn, pt_div)
cor.test(syn_mdl, pt_div_mdl)

# viz
new_time = seq(1, 17, 0.1)
syn_mdl = predict(gam(syn ~ s(time, k = 10)), newdata = data.frame(time = new_time))
pt_div_mdl = predict(gam(pt_div ~ s(time, k = 10)), newdata = data.frame(time = new_time))

plot(time, syn, type = "l", col = "#ffa50030", lwd = 3, xlab = "time", ylab = "syntheticity / phonotactic diversity", xaxt = "n", bty = "n", lty = 2)
lines(time, pt_div, type = "l", col = "#8b008b30", lwd = 3, lty = 3)
lines(new_time, syn_mdl, type = "l", col = "#ffa50095", lwd = 5)
lines(new_time, pt_div_mdl, type = "l", col = "#8b008b95", lwd = 5)
axis(1, at = seq(2, 16, 2), labels = 1100 + seq(2, 16, 2) * 50)

